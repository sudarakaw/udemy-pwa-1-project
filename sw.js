const
  // Service Worker version
  SW_VERSION = 1.1,
  STATIC_CACHE_ID = `giphygram-static-${SW_VERSION}`,
  MEDIA_CACHE_ID = 'giphygram-media',

  // Static cache of App Shell
  APP_ASSETS =
    [ 'index.html'
    , 'main.js'
    , 'images/flame.png'
    , 'images/logo.png'
    , 'images/sync.png'
    , 'vendor/bootstrap.min.css'
    , 'vendor/jquery.min.js'

    ],

  // Static cache strategy - Cache with Network fallback
  staticCache = (req, cache_id = STATIC_CACHE_ID) => caches
    .match(req)
    .then(cachedRes => {
      if(cachedRes) {
        return cachedRes
      }
      else {
        return fetch(req)
          .then(networkRes => {
            // Update cache with new response
            caches.open(cache_id)
              .then(cache => cache.put(req, networkRes))

            return networkRes.clone()
          })
      }
    }),

  // API cache strategy - Network with Cache fallback
  fallbackCache = req => fetch(req)
    // Network
    .then(networkRes => {
      if(!networkRes.ok) {
        // make a jump to `catch` block below
        throw new Error('fetch error')
      }

      // Update cache with new response
      caches.open(STATIC_CACHE_ID)
        .then(cache => cache.put(req, networkRes))

      return networkRes.clone()
    })

    // Cache
    .catch(/* err */ _ => caches.match(req)),

  // Clean old media from cache
  cleanMediaCache = current_list => {
    caches.open(MEDIA_CACHE_ID)
      .then(cache => {
        cache.keys().then(keys => {
          keys.forEach(key => {
            // if key (request) is NOT part of the `current_list`, Delete it.
            if(!current_list.includes(key.url)) {
              cache.delete(key)
            }
          })
        })
      })
  }

// SW Install
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(STATIC_CACHE_ID)
      .then(cache => cache.addAll(APP_ASSETS))
  )
})

// SW Activate
self.addEventListener('activate', e => {
  const
    // Clean static cache
    cleaned = caches.keys()
    .then(keys => {
      keys.forEach(key => {
        if(key !== STATIC_CACHE_ID && key.match('giphygram-static-')) {
          return caches.delete(key)
        }
      })
    })

  e.waitUntil(cleaned)
})

// SW Fetch
self.addEventListener('fetch', e => {
  // App shell
  if(e.request.url.match(location.origin)) {
    e.respondWith(staticCache(e.request))
  }

  // Giphy API
  else if(e.request.url.match('api.giphy.com/v1/gifs/trending')) {
    e.respondWith(fallbackCache(e.request))
  }

  // Giphy media
  else if(e.request.url.match('giphy.com/media')) {
    e.respondWith(staticCache(e.request, MEDIA_CACHE_ID))
  }
})

// Listen to messages from client
self.addEventListener('message', e => {
  // Identify message type
  if('cleanMediaCache' === e.data.action) {
    cleanMediaCache(e.data.except || [])
  }
})
